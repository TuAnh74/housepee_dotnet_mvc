﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housepee_asp.Models;
using housepee_asp.ViewModel;
using housepee_asp.Services;
namespace housepee_asp.Controllers
{
    public class UsersController : Controller
    {
        private HousepeeDBContext db = new HousepeeDBContext();

        // GET: Users
        public ActionResult Index()
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View(db.user.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (id == Convert.ToInt32(Session["UserId"]))
            {
                return View(user);

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Save(int SaveId)
        {
            if(Session["UserId"]!=null)
            {
                invoice input = new invoice();
                input.user_id = Convert.ToInt16(Session["UserId"]);
                input.room_id = SaveId;
                db.invoice.Add(input);
                db.SaveChanges();
                return RedirectToAction("Index", "RoomDetail");
            }
            else
            {
                 return RedirectToAction("Login", "Home");

            }

        }

        public ActionResult RoomDetail()
        {
            var model = new UserService().GetListRoomByUserId(Convert.ToInt32(Session["UserId"]));
            if (Session["Username"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View(model);

            //return View();
        }

        public ActionResult ChangePassWord()
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassWord(string password,string newPassword)
        {
            var status = new UserService().ChangePassWord(password, newPassword, Convert.ToString(Session["Username"]));
            if(Session["Username"]==null)
            {
                return RedirectToAction("Login", "Home");
            }
            if(status==true)
            {
                ViewBag.Mess = "Đổi mật khẩu thành công";
                return View();
            }
            else
            {
                ViewBag.Mess = "Thất bại! Vui lòng kiểm tra lại mật khẩu";
                return View();
            }
        }
        public ActionResult Invoice()
        {
            var model = new UserService().GetInVoiceByUserId(Convert.ToInt32(Session["UserId"]));
            return View(model);
        }

        public ActionResult BlogList()
        {
            var model = new UserService().GetBlogListByUserId(Convert.ToInt32(Session["UserId"]));
            return View(model);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "user_id,password,name_user,username,location,email,phonenumber,isadmin,blog_id,avatar")] user user)
        {
            if (ModelState.IsValid)
            {
                string encryptMD5Pass = Common.EncryptMD5(user.username + user.password);
                user.password = encryptMD5Pass;
                db.user.Add(user);
                db.SaveChanges();
                return RedirectToAction("Login", "Home");

            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "user_id,password,name_user,username,location,email,phonenumber,isadmin,blog_id,avatar")] user user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            user user = db.user.Find(id);
            db.user.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
