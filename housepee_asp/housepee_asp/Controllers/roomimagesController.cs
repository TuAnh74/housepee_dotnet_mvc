﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housepee_asp.Models;

namespace housepee_asp.Controllers
{
    public class roomimagesController : Controller
    {
        private HousepeeDBContext db = new HousepeeDBContext();

        // GET: roomimages
        public ActionResult Index()
        {
            var roomimages = db.roomimages.Include(r => r.roomdetail);
            return View(roomimages.ToList());
        }

        // GET: roomimages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            roomimages roomimages = db.roomimages.Find(id);
            if (roomimages == null)
            {
                return HttpNotFound();
            }
            return View(roomimages);
        }

        // GET: roomimages/Create
        public ActionResult Create()
        {
            ViewBag.room_id = new SelectList(db.roomdetail, "room_id", "name");
            return View();
        }

        // POST: roomimages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "room_image_id,room_id,url")] roomimages roomimages)
        {
            if (ModelState.IsValid)
            {
                db.roomimages.Add(roomimages);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.room_id = new SelectList(db.roomdetail, "room_id", "name", roomimages.room_id);
            return View(roomimages);
        }

        // GET: roomimages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            roomimages roomimages = db.roomimages.Find(id);
            if (roomimages == null)
            {
                return HttpNotFound();
            }
            ViewBag.room_id = new SelectList(db.roomdetail, "room_id", "name", roomimages.room_id);
            return View(roomimages);
        }

        // POST: roomimages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "room_image_id,room_id,url")] roomimages roomimages)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roomimages).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.room_id = new SelectList(db.roomdetail, "room_id", "name", roomimages.room_id);
            return View(roomimages);
        }

        // GET: roomimages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            roomimages roomimages = db.roomimages.Find(id);
            if (roomimages == null)
            {
                return HttpNotFound();
            }
            return View(roomimages);
        }

        // POST: roomimages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            roomimages roomimages = db.roomimages.Find(id);
            db.roomimages.Remove(roomimages);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
