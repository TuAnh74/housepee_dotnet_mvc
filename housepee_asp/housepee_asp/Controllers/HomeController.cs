﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using housepee_asp.Models;
using housepee_asp.Services;
using housepee_asp.ViewModel;
using System.Globalization;
namespace housepee_asp.Controllers
{
    public class HomeController : Controller
    {
        HousepeeDBContext db = new HousepeeDBContext();

        [HttpPost]
        public JsonResult GetDistrictList(string cityid)
        {
            var result = db.quanhuyen.Where(x => x.matp == cityid).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetSubDistrictList(string districtid)
        {
            var result = db.xaphuongthitran.Where(x => x.maqh == districtid).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetStreet(string subdistrictid)
        {
            var result = db.duong.Where(x => x.xaid == subdistrictid).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {

            ViewBag.CityList = db.tinhthanhpho.ToList();
            ViewBag.DistrictList= new SelectList(db.quanhuyen, "maqh", "name");
            ViewBag.SubDistrictList= new SelectList(db.xaphuongthitran, "xaid", "name");
            ViewBag.StreetList= new SelectList(db.duong, "maduong", "name");
            var model = new HomePageService().HomePage();
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Logout()
        {
            Session["Username"] = null;
            Session["IsAdmin"] = null;
            Session["UserId"] = null;
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Login()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            string passwordMD5 = Common.EncryptMD5(username + password);
            if (ModelState.IsValid)
            {
                UserService userLogin = new UserService();
                var checkLogin = userLogin.Login(username, passwordMD5);
                if (checkLogin != null)
                {
                    //userSession.UserName = loginModel.UserName;
                    //userSession.PassWord = loginModel.PassWord;
                    Session["Username"] = checkLogin.UserName;
                    Session["IsAdmin"] = checkLogin.IsAdmin;
                    Session["UserId"] = checkLogin.UserId;
                    TempData["Login"] = "<script>alert('Đăng nhập thành công');</script>";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    //ModelState.AddModelError("", "Đăng nhập không thành công");
                    ViewBag.error = "Đăng nhập sai hoặc bạn không có quyền vào";
                    TempData["Login"] = "<script>alert('Đăng nhập không thành công');</script>";

                }

            }
            return View();
        }
    }
}