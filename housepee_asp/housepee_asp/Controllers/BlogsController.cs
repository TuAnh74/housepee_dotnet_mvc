﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housepee_asp.Models;

namespace housepee_asp.Controllers
{
    public class BlogsController : Controller
    {
        private HousepeeDBContext db = new HousepeeDBContext();

        // GET: Blogs
        public ActionResult Index()
        {
            var blogs = db.blogs.Include(b => b.user);
            return View(blogs.ToList());
        }

        // GET: Blogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.user_id = new SelectList(db.user, "user_id", "name_user");
            blogs blogs = db.blogs.Find(id);
            if (blogs == null)
            {
                return HttpNotFound();
            }
            return View(blogs);
        }

        // GET: Blogs/Create
        public ActionResult Create()
        {
            ViewBag.user_id = new SelectList(db.user, "user_id", "password");
            if (Session["UserId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");

            }

        }

        // POST: Blogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "blog_id,title,description,content,image,createTime,user_id")] blogs blogs)
        {
            blogs.user_id = Convert.ToInt32(Session["UserId"]);
            blogs.createTime = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.blogs.Add(blogs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.user_id = new SelectList(db.user, "user_id", "password", blogs.user_id);
            return View(blogs);
        }

        // GET: Blogs/Edit/5

         public ActionResult Search (string searchkey)
        {
            var model = (from a in db.blogs.AsEnumerable()
                         where a.title == searchkey
                         select a).ToList();
            return View("Index", model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blogs blogs = db.blogs.Find(id);
            if (blogs == null)
            {
                return HttpNotFound();
            }
            ViewBag.user_id = new SelectList(db.user, "user_id", "password", blogs.user_id);
            return View(blogs);
        }

        // POST: Blogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "blog_id,title,description,content,image,createTime,user_id")] blogs blogs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blogs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.user_id = new SelectList(db.user, "user_id", "password", blogs.user_id);
            return View(blogs);
        }

        // GET: Blogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blogs blogs = db.blogs.Find(id);
            if (blogs == null)
            {
                return HttpNotFound();
            }
            return View(blogs);
        }

        // POST: Blogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            blogs blogs = db.blogs.Find(id);
            db.blogs.Remove(blogs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
