﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housepee_asp.Models;
using System.IO;
namespace housepee_asp.Controllers
{
    public class DemoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            HousepeeDBContext db = new HousepeeDBContext();
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path2 = Server.MapPath("~/images/users/" + Session["UserId"]);
                    if(!System.IO.Directory.Exists(path2))
                    {
                        Directory.CreateDirectory(path2);

                    }
                    string path = Path.Combine(Server.MapPath("~/images/users/" + Session["UserId"]),
                                               Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    //Save url images in database
                    roomimages input = new roomimages();
                    input.url = "/images/users/" + Session["UserId"]+"/" + file.FileName;
                    input.room_id = 1005;
                    db.roomimages.Add(input);
                    db.SaveChanges();
                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return View();
        }
    }
}