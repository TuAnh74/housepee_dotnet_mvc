﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housepee_asp.Models;

namespace housepee_asp.Controllers
{
    public class ContactsController : Controller
    {
        private HousepeeDBContext db = new HousepeeDBContext();

        
        // GET: Contacts/Create
        public ActionResult Create()
        {
            return View();
        }
            
        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,email,comment,phone_number")] contact contact)
        {
            if (ModelState.IsValid)
            {
                db.contact.Add(contact);
                db.SaveChanges();
                ViewBag.successMessage = "Success";
                return RedirectToAction("Create");
            }

            return View(Create());
        }

        // GET: Contacts/Edit/5
       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
