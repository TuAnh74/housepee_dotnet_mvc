﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housepee_asp.Models;
using housepee_asp.ViewModel;
using housepee_asp.Services;
using Newtonsoft.Json;
using System.IO;

namespace housepee_asp.Controllers
{
    public class RoomDetailController : Controller
    {
        private HousepeeDBContext db = new HousepeeDBContext();

        // GET: RoomDetail
        public ActionResult Index()
        {
            ViewBag.CityList = new SelectList(db.tinhthanhpho, "matp", "name");
            ViewBag.DistrictList = new SelectList(db.quanhuyen, "maqh", "name");
            ViewBag.SubDistrictList = new SelectList(db.xaphuongthitran, "xaid", "name");
            ViewBag.StreetList = new SelectList(db.duong, "maduong", "name");
            var model = new RoomService().ListRoomImage();
            //Distint list
            var newmodel = new RoomService().DistinctList(model);
            //End distint list
            return View(newmodel);
        }

        // GET: RoomDetail/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new RoomImageService().RoomImageDetailById(id);
            //var model2 = new RoomService().RoomTypeById(id);
            //RoomDetailViewModel roomDetailViewModel = db.RoomDetailViewModels.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }



        public ActionResult Search(string city, string distric, string sub_distric, string street, int? up, int? down)
        {


            var model = (from a in db.roomdetail.AsEnumerable()
                         join b in db.roomimages.AsEnumerable().DefaultIfEmpty()
                            on a.room_id equals b.room_id
                         join c in db.roomtype.AsEnumerable() on a.room_type_id equals c.room_type_id
                         join d in db.user on a.user_id equals d.user_id
                         select new RoomDetailViewModel()
                         {
                             id = a.room_id,
                             image = b.url,
                             title = a.name,
                             city = a.city,
                             sub_distric = a.sub_distric,
                             distric = a.distric,
                             street = a.street,
                             number = a.number,
                             price_room = a.price_room,
                             area = a.area,
                             createTime = a.createTime,
                             toilet = a.toilet,
                             Name_user = d.name_user,
                             name_type_room = c.name_type_room,

                         }).ToList();
            if (city.Length > 0)
            {
                model.Where(x => x.city == city).ToList();
                if (distric.Length > 0)
                {
                    model.Where(x => x.distric == distric).ToList();
                    if (sub_distric.Length > 0)
                    {
                        model.Where(x => x.sub_distric == sub_distric).ToList();
                    }
                    if (street.Length > 0)
                    {
                        model.Where(x => x.street == street).ToList();
                        if (up != null && down == null)
                        {
                            model.Where(x => x.price_room > up).ToList();
                        }
                        if (down != null && up == null)
                        {
                            model.Where(x => x.price_room > down).ToList();
                        }
                        if (down != null && up != null)
                        {
                            model.Where(x => x.price_room > down && x.price_room > up).ToList();
                        }
                    }
                }
            }

            //Distint list
            var newmodel = new RoomService().DistinctList(model);
            //End distint list

            return View("Index", newmodel);
        }

        public ActionResult Create()
        {
            if (Session["UserId"] != null)
            {
                ViewBag.room_type_id = new SelectList(db.roomtype, "room_type_id", "name_type_room");
                return View();
            }

            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: RoomDetail/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoomDetailViewModelForUpLoad roomDetailViewModel)
        {
            if (ModelState.IsValid)
            {
                roomdetail inputRoomDetail = new roomdetail();
                inputRoomDetail.price_room = roomDetailViewModel.price_room;
                inputRoomDetail.price_water = roomDetailViewModel.price_water;
                inputRoomDetail.price_electric = roomDetailViewModel.price_electric;
                inputRoomDetail.name = roomDetailViewModel.title;
                inputRoomDetail.description = roomDetailViewModel.description;
                inputRoomDetail.area = Convert.ToInt32(roomDetailViewModel.area);
                inputRoomDetail.services = roomDetailViewModel.services;
                inputRoomDetail.createTime = DateTime.Now;
                inputRoomDetail.room_type_id = roomDetailViewModel.room_type_id;
                inputRoomDetail.toilet = roomDetailViewModel.toilet;
                inputRoomDetail.bedroom = roomDetailViewModel.bedroom;
                inputRoomDetail.distric = roomDetailViewModel.distric;
                inputRoomDetail.number = roomDetailViewModel.number;
                inputRoomDetail.createBy = Session["Username"].ToString();
                inputRoomDetail.sub_distric = roomDetailViewModel.sub_distric;
                inputRoomDetail.city = roomDetailViewModel.city;
                inputRoomDetail.street = roomDetailViewModel.street;
                inputRoomDetail.user_id = Convert.ToInt32(Session["UserId"]);
                db.roomdetail.Add(inputRoomDetail);
                db.SaveChanges();
                var idInputRoomDetail = inputRoomDetail.room_id;
                if (roomDetailViewModel.File != null && roomDetailViewModel.File.ContentLength > 0)
                    try
                    {
                        string path2 = Server.MapPath("~/images/users/" + Session["UserId"]);
                        if (!System.IO.Directory.Exists(path2))
                        {
                            Directory.CreateDirectory(path2);

                        }
                        string path = Path.Combine(Server.MapPath("~/images/users/" + Session["UserId"]),
                                                   Path.GetFileName(roomDetailViewModel.File.FileName));
                        roomDetailViewModel.File.SaveAs(path);
                        //Save url images in database
                        roomimages input = new roomimages();
                        input.url = "/images/users/" + Session["UserId"] + "/" + roomDetailViewModel.File.FileName;
                        input.room_id = idInputRoomDetail;
                        db.roomimages.Add(input);
                        db.SaveChanges();
                        ViewBag.Message = "File uploaded successfully";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                else
                {
                    ViewBag.Message = "You have not specified a file.";
                }

                return RedirectToAction("Index");
            }
            //ViewBag.room_type_id = new SelectList(db.roomtype, "room_type_id", "name_type_room", roomDetailViewModel.room_type_id);
            return View(roomDetailViewModel);
        }
        // GET: RoomDetail/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new RoomService().ListRoomImageById(id);
            //RoomDetailViewModel roomDetailViewModel = db.RoomDetailViewModels.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: RoomDetail/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,image,description,services,price_room,price_water,price_electric,toilet,bedroom,title,city,sub_distric,distric,street,number,area,createTime,lastModifieldBy,lastModifieldTime,room_type_id,user_id")] RoomDetailViewModel roomDetailViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roomDetailViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roomDetailViewModel);
        }

        // GET: RoomDetail/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new RoomService().ListRoomImageById(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: RoomDetail/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RoomDetailViewModel roomDetailViewModel = db.RoomDetailViewModels.Find(id);
            db.RoomDetailViewModels.Remove(roomDetailViewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Change status when user click button save
        /// </summary>
        /// <param name="userId">User id </param>
        /// <param name="roomId">Room id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ChangeStatus(int userId, int roomId)
        {
            var result = new RoomService().GetInvoiceById(userId, roomId);
            return Json(new
            {
                status = result
            });
        }

        [HttpPost]
        public JsonResult StatusRoomOfUser(int userId)
        {
            var result = new RoomService().GetListInvoiceByUserId(userId);
            return Json(new
            {
                list = result.Select(x => x.room_id)
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
