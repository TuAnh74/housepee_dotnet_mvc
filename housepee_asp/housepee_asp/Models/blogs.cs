namespace housepee_asp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class blogs
    {
        [Key]
        public int blog_id { get; set; }

        [StringLength(100)]
        [Required]
        public string title { get; set; }

        [StringLength(1050)]
        [Required]
        public string content { get; set; }

        [StringLength(1050)]
        public string description { get; set; }

        [StringLength(1050)]
        public string image { get; set; }

        public DateTime? createTime { get; set; }

        public int user_id { get; set; }

        public virtual user user { get; set; }
    }
}
