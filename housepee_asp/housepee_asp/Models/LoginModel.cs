﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace housepee_asp.Models
{
    public partial class LoginModel
    {   
        //[Display(Name ="Tên đang nhập")]
        //[Required(ErrorMessage ="Bạn phải nhập tên đăng nhập")]
        public string UserName { get; set; }

        //[Display(Name ="Mật khẩu")]
        //[Required(ErrorMessage ="Bạn phải nhập password")]
        public string PassWord { get; set; }

        public int? IsAdmin { get; set; }

        public int? UserId { get; set; }
    }
}