namespace housepee_asp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class roomimages
    {
        [Key]
        public int room_image_id { get; set; }

        public int room_id { get; set; }

        [Column(TypeName = "text")]
        public string url { get; set; }

        public virtual roomdetail roomdetail { get; set; }
    }
}
