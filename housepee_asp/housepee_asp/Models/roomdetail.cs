namespace housepee_asp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("roomdetail")]
    public partial class roomdetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public roomdetail()
        {
            invoice = new HashSet<invoice>();
            review = new HashSet<review>();
            roomimages = new HashSet<roomimages>();
        }

        [Key]
        public int room_id { get; set; }

        [StringLength(50)]
        public string name { get; set; }

        [StringLength(50)]
        public string description { get; set; }

        public int? area { get; set; }

        [StringLength(250)]
        public string services { get; set; }

        public double? price_room { get; set; }

        public double? price_water { get; set; }

        public double? price_electric { get; set; }

        public int? toilet { get; set; }

        public int? bedroom { get; set; }

        [StringLength(50)]
        public string city { get; set; }

        [StringLength(50)]
        public string distric { get; set; }

        [StringLength(50)]
        public string sub_distric { get; set; }

        [StringLength(50)]
        public string street { get; set; }

        [Column(TypeName = "text")]
        public string number { get; set; }

        [StringLength(50)]
        public string createBy { get; set; }

        public DateTime? createTime { get; set; }

        [StringLength(10)]
        public string lastModifieldBy { get; set; }

        public DateTime? lastModifieldTime { get; set; }

        public int? room_type_id { get; set; }

        public int? user_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<invoice> invoice { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<review> review { get; set; }

        public virtual roomtype roomtype { get; set; }

        public virtual user user { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<roomimages> roomimages { get; set; }
    }
}
