namespace housepee_asp.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class HousepeeDBContext : DbContext
    {
        public HousepeeDBContext()
            : base("name=HousepeeDBContext")
        {
        }

        public virtual DbSet<blogs> blogs { get; set; }
        public virtual DbSet<contact> contact { get; set; }
        public virtual DbSet<invoice> invoice { get; set; }
        public virtual DbSet<review> review { get; set; }
        public virtual DbSet<roomdetail> roomdetail { get; set; }
        public virtual DbSet<roomimages> roomimages { get; set; }
        public virtual DbSet<roomtype> roomtype { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<duong> duong { get; set; }
        public virtual DbSet<quanhuyen> quanhuyen { get; set; }
        public virtual DbSet<tinhthanhpho> tinhthanhpho { get; set; }
        public virtual DbSet<xaphuongthitran> xaphuongthitran { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<blogs>()
                .Property(e => e.title)
                .IsUnicode(false);

            modelBuilder.Entity<blogs>()
                .Property(e => e.content)
                .IsUnicode(false);

            modelBuilder.Entity<blogs>()
                .Property(e => e.image)
                .IsUnicode(false);

            //modelBuilder.Entity<contact>()
            //    .Property(e => e.id)
            //    .IsFixedLength();

            modelBuilder.Entity<contact>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<contact>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<review>()
                .Property(e => e.comment)
                .IsUnicode(false);

            modelBuilder.Entity<roomdetail>()
                .Property(e => e.number)
                .IsUnicode(false);

            modelBuilder.Entity<roomdetail>()
                .Property(e => e.lastModifieldBy)
                .IsFixedLength();

            modelBuilder.Entity<roomdetail>()
                .HasMany(e => e.invoice)
                .WithRequired(e => e.roomdetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<roomdetail>()
                .HasMany(e => e.review)
                .WithRequired(e => e.roomdetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<roomdetail>()
                .HasMany(e => e.roomimages)
                .WithRequired(e => e.roomdetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<roomimages>()
                .Property(e => e.url)
                .IsUnicode(false);

            modelBuilder.Entity<roomtype>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.phonenumber)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .HasMany(e => e.blogs)
                .WithRequired(e => e.user)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<user>()
                .HasMany(e => e.invoice)
                .WithRequired(e => e.user)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<user>()
                .HasMany(e => e.review)
                .WithRequired(e => e.user)
                .WillCascadeOnDelete(false);
        }

        public System.Data.Entity.DbSet<housepee_asp.ViewModel.RoomDetailViewModel> RoomDetailViewModels { get; set; }
    }
}
