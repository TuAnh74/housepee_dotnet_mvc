namespace housepee_asp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("quanhuyen")]
    public partial class quanhuyen
    {
        [Key]
        [StringLength(5)]
        public string maqh { get; set; }

        [Required]
        [StringLength(100)]
        public string name { get; set; }

        [Required]
        [StringLength(30)]
        public string type { get; set; }

        [Required]
        [StringLength(5)]
        public string matp { get; set; }
    }
}
