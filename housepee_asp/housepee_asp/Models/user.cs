namespace housepee_asp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("user")]
    public partial class user
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public user()
        {
            blogs = new HashSet<blogs>();
            invoice = new HashSet<invoice>();
            review = new HashSet<review>();
            roomdetail = new HashSet<roomdetail>();
        }

        [Key]
        public int user_id { get; set; }



        [StringLength(200)]
        public string password { get; set; }

        [StringLength(50)]
        public string name_user { get; set; }

        [StringLength(50)]
        public string username { get; set; }

        [StringLength(150)]
        public string location { get; set; }

        [StringLength(50)]
        public string email { get; set; }

        [StringLength(12)]
        public string phonenumber { get; set; }

        public int? isadmin { get; set; }

        public int? blog_id { get; set; }

        [StringLength(350)]
        public string avatar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<blogs> blogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<invoice> invoice { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<review> review { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<roomdetail> roomdetail { get; set; }
    }
}
