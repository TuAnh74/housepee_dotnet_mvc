namespace housepee_asp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("review")]
    public partial class review
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public int user_id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int room_id { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string comment { get; set; }

        public virtual roomdetail roomdetail { get; set; }

        public virtual user user { get; set; }
    }
}
