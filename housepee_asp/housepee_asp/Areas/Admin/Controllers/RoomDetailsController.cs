﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using housepee_asp.Models;
using housepee_asp.ViewModel;
using housepee_asp.Services;
namespace housepee_asp.Areas.Admin.Controllers
{
    public class RoomDetailsController : Controller
    {
        private HousepeeDBContext db = new HousepeeDBContext();

        // GET: Admin/RoomDetails
        public ActionResult Index()
        {
            var roomdetail = db.roomdetail.Include(r => r.roomtype).Include(r => r.user);
            return View(roomdetail.ToList());
        }

        // GET: Admin/RoomDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new RoomService().ListRoomImageById(id);
            //RoomDetailViewModel roomDetailViewModel = db.RoomDetailViewModels.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: Admin/RoomDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/RoomDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoomDetailViewModel roomDetailViewModel)
        {
            if (ModelState.IsValid)
            {
                roomdetail inputRoomDetail = new roomdetail();
                inputRoomDetail.price_room = roomDetailViewModel.price_room;
                inputRoomDetail.price_water = roomDetailViewModel.price_water;
                inputRoomDetail.price_electric = roomDetailViewModel.price_electric;
                inputRoomDetail.name = roomDetailViewModel.title;
                inputRoomDetail.description = roomDetailViewModel.description;
                inputRoomDetail.area = Convert.ToInt32(roomDetailViewModel.area);
                inputRoomDetail.services = roomDetailViewModel.services;
                inputRoomDetail.createTime = DateTime.Now;
                inputRoomDetail.room_type_id = roomDetailViewModel.room_type_id;
                inputRoomDetail.toilet = roomDetailViewModel.toilet;
                inputRoomDetail.bedroom = roomDetailViewModel.bedroom;
                inputRoomDetail.distric = roomDetailViewModel.distric;
                inputRoomDetail.sub_distric = roomDetailViewModel.sub_distric;
                inputRoomDetail.city = roomDetailViewModel.city;
                inputRoomDetail.street = roomDetailViewModel.street;


                db.roomdetail.Add(inputRoomDetail);
                //db.RoomDetailViewModels.Add(roomDetailViewModel);
                db.SaveChanges();
                var newRoomDetail = (from a in db.roomdetail.AsEnumerable()
                                     where a.user_id == roomDetailViewModel.user_id
                                     && a.createTime == roomDetailViewModel.createTime
                                     select new roomdetail()
                                     {
                                         room_id = a.room_id,
                                     }).FirstOrDefault();
                roomimages roomimages = new roomimages();
                roomimages.room_id = newRoomDetail.room_id;
                roomimages.url = roomDetailViewModel.image;
                db.roomimages.Add(roomimages);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(roomDetailViewModel);
        }

        // GET: Admin/RoomDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new RoomService().ListRoomImageById(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Admin/RoomDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,image,description,services,price_room,price_water,price_electric,toilet,bedroom,title,city,sub_distric,distric,street,number,area,createTime,lastModifieldBy,lastModifieldTime,room_type_id,user_id,name_type_room")] RoomDetailViewModel roomDetailViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roomDetailViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roomDetailViewModel);
        }

        // GET: Admin/RoomDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new RoomService().ListRoomImageById(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Admin/RoomDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RoomDetailViewModel roomDetailViewModel = db.RoomDetailViewModels.Find(id);
            db.RoomDetailViewModels.Remove(roomDetailViewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
