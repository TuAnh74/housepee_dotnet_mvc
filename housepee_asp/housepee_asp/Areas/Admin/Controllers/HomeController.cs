﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace housepee_asp.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            if (Convert.ToInt32(Session["IsAdmin"]) == 1)

                return View();
            else
                return Redirect("/home");
        }
    }
}