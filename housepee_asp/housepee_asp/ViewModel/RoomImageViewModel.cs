﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using housepee_asp.Models;

namespace housepee_asp.ViewModel
{
    public partial class RoomImageViewModel
    {
        public List<roomimages> ListImage { get; set; }

        public RoomDetailViewModel RoomDeTail { get; set; }
    }
}