﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using housepee_asp.Models;
namespace housepee_asp.ViewModel
{
    public partial class HomePageViewModel
    {
        public List<blogs> Blog { get; set; }

        public List<RoomDetailViewModel> RoomDetail { get; set; }
    }
}