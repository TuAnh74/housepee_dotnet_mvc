﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using housepee_asp.Models;
namespace housepee_asp.ViewModel
{
    public partial class RoomDetailViewModel
    {

        public int id { get; set; }

        public string image { get; set; }

        public string description { get; set; }

        public string services { get; set; }

        public double? price_room { get; set; }

        public double? price_water { get; set; }

        public double? price_electric { get; set; }

        public int? toilet { get; set; }

        public int? bedroom { get; set; }

        public string title { get; set; }

        public string city { get; set; }

        public string sub_distric { get; set; }

        public string distric { get; set; }

        public string street { get; set; }

        public string number { get; set; }

        public double? area { get; set; }

        public DateTime? createTime { get; set; }

        public string lastModifieldBy { get; set; }

        public DateTime? lastModifieldTime { get; set; }

        public int? room_type_id { get; set; }

        public int? user_id { get; set; }

        public string name_type_room { get; set; }

        public string Name_user { get; set; }

        public string Number_phone { get; set; }
    }
    public partial class RoomDetailViewModelForUpLoad
    {
        public int id { get; set; }

        public string image { get; set; }

        public string description { get; set; }

        public string services { get; set; }

        public double? price_room { get; set; }

        public double? price_water { get; set; }

        public double? price_electric { get; set; }

        public int? toilet { get; set; }

        public int? bedroom { get; set; }

        public string title { get; set; }

        public string city { get; set; }

        public string sub_distric { get; set; }

        public string distric { get; set; }

        public string street { get; set; }

        public string number { get; set; }

        public double? area { get; set; }

        public DateTime? createTime { get; set; }

        public string lastModifieldBy { get; set; }

        public DateTime? lastModifieldTime { get; set; }

        public int? room_type_id { get; set; }

        public int? user_id { get; set; }

        public string name_type_room { get; set; }

        public string Name_user { get; set; }

        public string Number_phone { get; set; }

        public HttpPostedFileBase File { get; set; }
    }
}