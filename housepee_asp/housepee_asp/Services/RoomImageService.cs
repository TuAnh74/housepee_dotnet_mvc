﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using housepee_asp.Models;
using housepee_asp.Services.Interfaces;
using housepee_asp.ViewModel;
namespace housepee_asp.Services
{
    public class RoomImageService : IRoomImageService
    {
        private HousepeeDBContext db = new HousepeeDBContext();
        public RoomImageViewModel RoomImageDetailById(int? id)
        {
            RoomImageViewModel model = new RoomImageViewModel();
            model.RoomDeTail = (from a in db.roomdetail.AsEnumerable()
                                join b in db.roomimages.AsEnumerable()
                                   on a.room_id equals b.room_id
                                join c in db.roomtype.AsEnumerable() on a.room_type_id equals c.room_type_id
                                join d in db.user on a.user_id equals d.user_id
                                where a.room_id == id
                                select new RoomDetailViewModel()
                                {
                                    name_type_room = c.name_type_room,
                                    id = a.room_id,
                                    image = b.url,
                                    title = a.name,
                                    city = a.city,
                                    sub_distric = a.sub_distric,
                                    distric = a.distric,
                                    street = a.street,
                                    number = a.number,
                                    price_room = a.price_room,
                                    area = a.area,
                                    createTime = a.createTime,
                                    toilet = a.toilet,
                                    bedroom = a.bedroom,
                                    price_electric = a.price_electric,
                                    price_water = a.price_water,
                                    description = a.description,
                                    services = a.services,
                                    Number_phone = d.phonenumber,
                                    Name_user = d.name_user,
                                }).FirstOrDefault();
            model.ListImage = (from a in db.roomimages.AsEnumerable()
                               where a.room_id == id
                               select a).ToList();
            return model;
        }
    }
}