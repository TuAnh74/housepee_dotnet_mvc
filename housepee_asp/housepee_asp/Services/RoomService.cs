﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using housepee_asp.Models;
using housepee_asp.Services.Interfaces;
using housepee_asp.ViewModel;
namespace housepee_asp.Services
{
    public class RoomService : IRoomService
    {
        private HousepeeDBContext db = new HousepeeDBContext();

        public List<RoomDetailViewModel> DistinctList(List<RoomDetailViewModel> model)
        {
            var newmodel = model;
            for (int i = 0; i < model.Count() - 1; i++)
            {
                for (int j = i + 1; j < model.Count(); j++)
                {
                    if (model[i].id == model[j].id)
                        newmodel.Remove(newmodel[j]);
                }
            }
            //End distint list
            return newmodel;
        }
        public List<RoomDetailViewModel> ListRoomImage()
        {
            var model = (from a in db.roomdetail.AsEnumerable()
                         join b in db.roomimages.AsEnumerable().DefaultIfEmpty()
                            on a.room_id equals b.room_id
                         join c in db.roomtype.AsEnumerable() on a.room_type_id equals c.room_type_id
                         join d in db.user on a.user_id equals d.user_id
                         select new RoomDetailViewModel()
                         {
                             id = a.room_id,
                             image = b.url,
                             title = a.name,
                             city = a.city,
                             sub_distric = a.sub_distric,
                             distric = a.distric,
                             street = a.street,
                             number = a.number,
                             price_room = a.price_room,
                             area = a.area,
                             createTime = a.createTime,
                             toilet=a.toilet,
                             Name_user = d.name_user,
                             name_type_room = c.name_type_room,

                         }).ToList();
            //Distint list
            var newmodel=DistinctList(model);
            //End distint list
            return newmodel;
        }
        public RoomDetailViewModel ListRoomImageById(int? id)
        {
            var model = (from a in db.roomdetail.AsEnumerable()
                         join b in db.roomimages.AsEnumerable()
                            on a.room_id equals b.room_id
                         join c in db.roomtype.AsEnumerable() on a.room_type_id equals c.room_type_id
                         join d in db.user on a.user_id equals d.user_id
                         where a.room_id == id 
                         select new RoomDetailViewModel()
                         {
                             name_type_room=c.name_type_room,
                             id = a.room_id,
                             image = b.url,
                             title = a.name,
                             city = a.city,
                             sub_distric = a.sub_distric,
                             distric = a.distric,
                             street = a.street,
                             number = a.number,
                             price_room = a.price_room,
                             area = a.area,
                             createTime = a.createTime,
                             toilet = a.toilet,
                             bedroom=a.bedroom,
                             price_electric=a.price_electric,
                             price_water=a.price_water,
                             description=a.description,
                             services = a.services,
                             Number_phone=d.phonenumber,
                             Name_user=d.name_user,
                             

                         }).FirstOrDefault(); ;

            return model;
        }
   
        public RoomDetailViewModel RoomTypeById(int? id)
        {
            var models = (from a in db.roomtype.AsEnumerable()
                          join b in db.roomdetail.AsEnumerable()
 on a.room_type_id equals b.room_type_id
                          where b.room_id == id
                          select new RoomDetailViewModel()
                          {
                              name_type_room = a.name_type_room,
                         }
                       ).FirstOrDefault();
            return models;
        } 

        public bool GetInvoiceById(int userId, int roomId)
        {
            var model = (from a in db.invoice.AsEnumerable()
                        where a.room_id == roomId && a.user_id == userId
                        select a).FirstOrDefault();
            if(model==null)
            {
                invoice input = new invoice();
                input.user_id = userId;
                input.room_id = roomId;
                db.invoice.Add(input);
                db.SaveChanges();
                return true;
            }
            else
            {
                db.invoice.Remove(model);
                db.SaveChanges();
                return false;
            }
        }

        public List<invoice> GetListInvoiceByUserId(int userId)
        {
            var models = (from a in db.invoice.AsEnumerable()
                          where a.user_id == userId
                          select a
                        ).ToList();
            return models;
        }
    }
}