﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using housepee_asp.ViewModel;
namespace housepee_asp.Services.Interfaces
{
    public interface IHomePageService
    {
        HomePageViewModel HomePage();
    }
}