﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using housepee_asp.Models;
using housepee_asp.ViewModel;
namespace housepee_asp.Services.Interfaces
{
    public interface IUserService
    {
        LoginModel Login(string username, string password);
        List<RoomDetailViewModel> GetListRoomByUserId(int? id);
        List<blogs> GetBlogListByUserId(int? id);
        List<invoice> GetInVoiceByUserId(int? id);
        bool ChangePassWord(string oldPassWord, string newPassWord, string username);
    }
}