﻿using housepee_asp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using housepee_asp.Models;
namespace housepee_asp.Services.Interfaces
{
    public interface IRoomService
    {
        List<RoomDetailViewModel> ListRoomImage();

        RoomDetailViewModel ListRoomImageById(int? id);

        bool GetInvoiceById(int userId, int roomId);

        List<invoice> GetListInvoiceByUserId(int userId);
    }
}
