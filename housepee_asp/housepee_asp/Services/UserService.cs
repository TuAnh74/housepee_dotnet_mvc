﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using housepee_asp.Models;
using housepee_asp.Services.Interfaces;
using housepee_asp.ViewModel;
using housepee_asp.Controllers;
namespace housepee_asp.Services
{
    public class UserService : IUserService
    {
        private HousepeeDBContext db = new HousepeeDBContext();

        public LoginModel Login(string username, string password)
        {
            var model = (from a in db.user.AsEnumerable()
                         where a.username == username && a.password == password
                         select new LoginModel
                         {
                             UserName = a.username,
                             PassWord = a.password,
                             IsAdmin = a.isadmin,
                             UserId = a.user_id,
                         }
                       ).FirstOrDefault();

            return model;
        }

        public bool ChangePassWord(string oldPassWord, string newPassWord, string username)
        {
            var encryptPassword = Common.EncryptMD5(username + oldPassWord);
            var model = db.user.Where(x => x.password == encryptPassword && x.username == username).Select(x => x).FirstOrDefault();
            if (model != null)
            {
                model.password = Convert.ToString(Common.EncryptMD5(username + newPassWord));
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<RoomDetailViewModel> GetListRoomByUserId(int? id)
        {
            var model = (from a in db.roomdetail.AsEnumerable()
                         join b in db.roomtype.AsEnumerable()
                         on a.room_type_id equals b.room_type_id
                         where a.user_id == id
                         select new RoomDetailViewModel()
                         {
                             name_type_room = b.name_type_room,
                             id = a.room_id,
                             title = a.name,
                             city = a.city,
                             sub_distric = a.sub_distric,
                             distric = a.distric,
                             street = a.street,
                             number = a.number,
                             price_room = a.price_room,
                             area = a.area,
                             createTime = a.createTime,
                             toilet = a.toilet,
                             bedroom = a.bedroom,
                             price_electric = a.price_electric,
                             price_water = a.price_water,
                             description = a.description,
                         }).ToList();

            return model;
        }

        public List<blogs> GetBlogListByUserId(int? id)
        {
            var model = (from a in db.blogs.AsEnumerable()
                         where a.user_id == id
                         select a
                         ).ToList();
            return model;
        }

        public List<invoice> GetInVoiceByUserId(int? id)
        {
            var model = (from a in db.invoice.AsEnumerable()
                         where a.user_id == id
                         select a).ToList();
            return model;
        }



    }
}