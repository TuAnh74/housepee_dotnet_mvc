﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using housepee_asp.Models;
using housepee_asp.Services.Interfaces;
using housepee_asp.ViewModel;
namespace housepee_asp.Services
{
    public class HomePageService : IHomePageService
    {
        private HousepeeDBContext db = new HousepeeDBContext();
        public HomePageViewModel HomePage()
        {
            HomePageViewModel model = new HomePageViewModel();
            model.Blog = (from a in db.blogs.AsEnumerable()
                          orderby a.createTime descending
                          select a).ToList();
            model.RoomDetail = (from a in db.roomdetail.AsEnumerable()
                                join b in db.roomimages.AsEnumerable().DefaultIfEmpty()
                                   on a.room_id equals b.room_id
                                join c in db.roomtype.AsEnumerable() on a.room_type_id equals c.room_type_id
                                join d in db.user on a.user_id equals d.user_id
                                orderby a.createTime descending
                                select new RoomDetailViewModel()
                                {
                                    id = a.room_id,
                                    image = b.url,
                                    title = a.name,
                                    city = a.city,
                                    sub_distric = a.sub_distric,
                                    distric = a.distric,
                                    street = a.street,
                                    number = a.number,
                                    price_room = a.price_room,
                                    area = a.area,
                                    createTime = a.createTime,
                                    toilet = a.toilet,
                                    Name_user = d.name_user,
                                    name_type_room = c.name_type_room,

                                }).ToList();


            //Distint list
            var newmodel = model;
            newmodel.RoomDetail=new RoomService().DistinctList(model.RoomDetail);
            //End distint list
            return newmodel;
        }

    }
}